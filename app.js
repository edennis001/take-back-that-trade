const { exec } = require('child_process');
const keypress = require('keypress');
const args = require('minimist')(process.argv.slice(2));
const AWS = require('aws-sdk');

//README HOW TO CALL node app.js -p ~/Desktop/1549385358944.json -d 2019-10-18 -n 2019-07-12 -i 1234566 -t 5
// get path variables otherwise throw exception
var path = args.p || args.path;
var dateOfTrade = args.d || args.dateoftrade;
var newDate = args.n || args.newdate;
var transactionTemplateId = args.i;
var tenantId = args.t;

if (path === undefined) {
  console.error('Path must be specified!');
  process.exit();
}

if (dateOfTrade === undefined) {
  console.error('Date of trade must be specified!');
  process.exit();
}

if (newDate === undefined) {
  console.error('New date of transaction must be specified!');
  process.exit();
}

if (transactionTemplateId === undefined) {
  console.error('Transaction template id  must be specified!');
  process.exit();
}

if (tenantId === undefined) {
  console.error('Tenant id  must be specified!');
  process.exit();
}

exec(`jq \'.value|map(select(.effectiveDate == "` + dateOfTrade +
  `" and .glaNumber == "120000-010001" 
    and.comments == "Settle Purchase of Investment. Reversing and trueing up purchased shares" 
    and.je_comments == null)
    |{ "@lots": [{ "proceeds":.Debit, "loss": 0.0, "gain": 0.0, "cost":.Debit, "units": (.shares | fabs), 
    "indicator": "UNITS", "number":.lot }], 
    "@txReference": "Contribution", "@reference":.je_ref, "@account":.number, 
    "@effectiveDate": "` + newDate + `", "@glDate": "` + newDate + `" })\' ` + path, (err, stdout, stderr) => {

    if (err)
      console.log(err);

    if (stderr)
      console.log(stderr);

    // add up all the cost values to make sure trades that will be reversed add up to contribution
    console.log(stdout);
    var resp = JSON.parse(stdout);
    var i = 0;
    var cost = 0;
    Object.keys(resp).forEach(function (key) {
      i++;
      cost += (resp[key]['@lots'][0].cost);
    });

    console.log('Total value of trade:', cost);
    console.log('Total numbers of trades that will be reversed:', i);
    console.log('Do you want to proceed with trade reversal? Please respond with \'Y\' or \'N\'');

    keypress(process.stdin);
    process.stdin.on('keypress', function (ch, key) {
      if (key && key.ctrl && key.name == 'c') {
        process.stdin.pause();
      }

      if (key && key.shift && key.name == 'y') {
        console.log('Proceeding..')
        process.stdin.pause();

        Object.keys(resp).forEach(function (key) {
          callService(resp[key]);
        })

      }

      if (key && key.shift && key.name == 'n') {
        console.log('You chose not to continue.. GAME OVER!')
        process.stdin.pause();
      }
    });


  });

function callService(req) {
  // TODO add transaction template for DEV or TEST parameterized this is different for each tenant 
  var region = 'us-east-1';
  var domain = 'api.prd.iralogix.com'; //change programmatically 

  var endpoint = new AWS.Endpoint(domain);
  var request = new AWS.HttpRequest(endpoint, region);

  request.method = 'POST';
  request.path = '/accounting/v4/transactionTemplate/'+transactionTemplateId+'/execute'; //TODO parameterize the transactionTempladeId maybe find a way to get programmatically
  request.body = JSON.stringify(req);
  request.headers['host'] = domain;
  request.headers['Content-Type'] = 'application/json';
  request.headers['x-tenant-id'] = tenantId; // TODO also do programmatically
  request.headers['Content-Length'] = Buffer.byteLength(request.body);

  /*
  picked up from environment variable figure out how out change programmatically maybe based on flag 
    AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY
    AWS_SESSION_TOKEN (optional)

    currently adding by running following commands
    $ export AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
    $ export AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
  */
  var credentials = new AWS.EnvironmentCredentials('AWS');
  var signer = new AWS.Signers.V4(request, 'execute-api');
  signer.addAuthorization(credentials, new Date());

  var client = new AWS.HttpClient();
  client.handleRequest(request, null, function (response) {
    console.log(response.statusCode + ' ' + response.statusMessage);
    var responseBody = '';
    response.on('data', function (chunk) {
      responseBody += chunk;
    });
    response.on('end', function (chunk) {
      console.log('Response body: ' + responseBody);
    });
  }, function (error) {
    console.log('Error: ' + error);
  });
}
